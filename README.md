# grabss

## Description
Bash script to take screen shots from your computer. 

## Depends On
	gxmessage
	zenity
	scrot
	feh

## Optional
Screenshot dir is stored in global variable GRABSSDIR
If variable does not exist, it will default to the users 
Home Directory.

## Installation
Download the script then place it into directory of your choice like
/usr/local/bin/ or /home/YOUR-USERNAME/bin/ -
Set permissions to 755

Now your're ready to grab screenshots.